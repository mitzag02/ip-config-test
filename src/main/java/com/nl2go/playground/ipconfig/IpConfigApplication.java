package com.nl2go.playground.ipconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpConfigApplication.class, args);
	}

}
